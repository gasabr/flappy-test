#include "Bird.h"
#include "SimpleAudioEngine.h"
#include "Definitions.h"

USING_NS_CC;

Bird::Bird()
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
}

void Bird::createBird( cocos2d::Layer *layer )
{
    flappySprite = Sprite::create( "iphonehd/UFO.png" );
    flappySprite->setPosition( Point( visibleSize.width / 4 + origin.x, visibleSize.height / 2 + origin.y ) );
    
    flappyBody = PhysicsBody::createCircle( flappySprite->getContentSize().width / 2 + 1 );
    flappyBody->setCollisionBitmask( BIRD_COLLISION_BITMASK );
    flappyBody->setContactTestBitmask( BIRD_CONTACT_MASK );
    
    flappySprite->setPhysicsBody( flappyBody );
    
    layer->addChild( flappySprite );
}

void Bird::Fly()
{    
    flappyBody->applyImpulse( Vec2(0, (GRAVITY + 6000)) );
    
    /*Doesn't look like the truth, but*/
// P = mv0;
// v0 = P / m = 85
// (v == Vec(0, -GRAVITY))
// v1 = Vec(0, v0 - GRAVITY); v1 = speed after aplying impulse P
}