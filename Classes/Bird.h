#ifndef __BIRD_H__
#define __BIRD_H__

#include "cocos2d.h"

class Bird
{
public:
    Bird();
    Bird( cocos2d::Layer *layer );
    void createBird( cocos2d::Layer *layer );
    
    void Fly();

private:
    cocos2d::Size visibleSize;
    cocos2d::Vec2 origin;
    
    cocos2d::Sprite *flappySprite;
    cocos2d::PhysicsBody *flappyBody;
};

#endif // __BIRD_H__
