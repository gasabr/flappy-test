#include "GameOverScene.h"
#include "SimpleAudioEngine.h"
#include "MainMenuScene.h"
#include "Definitions.h"
#include "GameScene.h"

USING_NS_CC;

unsigned int score;

Scene* GameOverScene::createScene(unsigned int tempScore)
{
    score = tempScore;
    log("create scene: score = %i", score);
    
    auto scene = Scene::create();
    auto layer = GameOverScene::create();
    scene->addChild(layer);

    return scene;
}

bool GameOverScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create( "iphonehd/Menu_Background.png" );
    backgroundSprite->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    this->addChild(backgroundSprite);
    
    auto mainMenuItem = MenuItemImage::create( "iphonehd/Menu_Button.png", "iphonehd/Menu_Button_Clicked.png",
                                         CC_CALLBACK_1( GameOverScene::GoToMainMenuScene , this ) );
    mainMenuItem->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height * 0.4 + origin.y ) );
    
/*Box2D button*/
    auto retryItem = MenuItemImage::create( "iphonehd/Retry_Button.png", "iphonehd/Retry_Button_Clicked.png",
                                               CC_CALLBACK_1(GameOverScene::Retry, this ) );
    
    retryItem->setPosition( Point( visibleSize.width / 2 + origin.x,
                                      mainMenuItem->getPositionY() - retryItem->getContentSize().height * 2));
    
    auto menu = Menu::create( mainMenuItem, retryItem, NULL );
    menu->setPosition( Point::ZERO );
    
    this->addChild( menu );
    
/*UFO Sprite*/
    auto UFOSprite = Sprite::create("iphonehd/UFO_crash.png");
    UFOSprite->setPosition( Vec2( visibleSize.width / 2 + origin.x, visibleSize.height * 0.8 + origin.y ) );
    this->addChild(UFOSprite);
    
/*Result Label*/
    auto scoreLabelStr = __String::createWithFormat("result: %u", score);
    
    auto scoreLabel = Label::createWithTTF( scoreLabelStr->getCString(), "fonts/Roboto-Light.ttf",
                                           visibleSize.height * FONT_SIZE );

    scoreLabel->setColor( Color3B::WHITE );
    scoreLabel->setPosition( Point( visibleSize.width / 2 + origin.x, UFOSprite->getPositionY() - scoreLabel->getContentSize().height * 3 ) );
    
    this->addChild( scoreLabel, 1000 );
    
    log("game scene: score = %u", score);
    
    return true;
}

void GameOverScene::GoToMainMenuScene( cocos2d::Ref *sender )
{
    auto scene = MainMenuScene::createScene();
    Director::getInstance()->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}

void GameOverScene::Retry( cocos2d::Ref *sender )
{
    auto scene = GameScene::createScene();
    Director::getInstance()->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}