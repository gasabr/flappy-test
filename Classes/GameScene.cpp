#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "GameOverScene.h"
#include "Definitions.h"
#include "Bird.h"

USING_NS_CC;

Scene* GameScene::createScene()
{
    auto scene = Scene::createWithPhysics();
    //scene->getPhysicsWorld()->setDebugDrawMask( PhysicsWorld::DEBUGDRAW_ALL );
    scene->getPhysicsWorld()->setGravity( Vect(0, -GRAVITY ) );
    
    auto layer = GameScene::create();
    layer->SetPhysicsWorld( scene->getPhysicsWorld() );
    
    scene->addChild(layer);
    
    return scene;
}

bool GameScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
/*Background sprite*/
    auto backgroundSprite = Sprite::create( "iphonehd/GameScene_Background.png" );
    backgroundSprite->setPosition( Point( visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y ) );
    this->addChild(backgroundSprite);
    
/*Screen edges physics*/
    auto edgeBody = PhysicsBody::createEdgeBox( visibleSize, PHYSICSBODY_MATERIAL_DEFAULT, 3 );
    edgeBody->setContactTestBitmask(true);
    edgeBody->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    
    auto edgeNode = Node::create();
    edgeNode->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height/2 + origin.y ) );
    edgeNode->setPhysicsBody( edgeBody );
    
    this->addChild( edgeNode );
    
/*spawn pipe*/
    this->schedule( schedule_selector( GameScene::SpawnPipe ), PIPE_SPAWN_FREQUENCY * visibleSize.width );
    
/*spawn bird*/
    bird.createBird( this );
    
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1( GameScene::onContactBegin, this );
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority( contactListener, this );
    
    auto touchListener = EventListenerTouchOneByOne::create();
    touchListener->setSwallowTouches( true );
    touchListener->onTouchBegan = CC_CALLBACK_2( GameScene::onTouchBegan, this );
    Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority( touchListener, this );
    
/*Count score*/
    score = 0;
    
    __String *temp_scoreString = __String::createWithFormat( "%i", score );
    
    scoreLabel = Label::createWithTTF( temp_scoreString->getCString(), "fonts/Roboto-Light.ttf",
                                      visibleSize.height * FONT_SIZE );
    scoreLabel->setColor( Color3B::WHITE );
    scoreLabel->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height * 0.75 + origin.y ) );
    
    this->addChild( scoreLabel, 1000 );
    
    this->scheduleUpdate();
    
    return true;
}

bool GameScene::onContactBegin( cocos2d::PhysicsContact &contact )
{
    PhysicsBody *a = contact.getShapeA()->getBody();
    PhysicsBody *b = contact.getShapeB()->getBody();
    
    if ( ( BIRD_COLLISION_BITMASK == a->getCollisionBitmask() && OBSTACLE_COLLISION_BITMASK == b->getCollisionBitmask() )
        || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask() && OBSTACLE_COLLISION_BITMASK == a->getCollisionBitmask() ) ){
        
        auto scene = GameOverScene::createScene(score);
        
        Director::getInstance()->replaceScene( TransitionFade::create( CRASH_TRANSITION_TIME, scene ) );
    }
    else if (( BIRD_COLLISION_BITMASK == a->getCollisionBitmask() && SCORE_POINT_COLLISION_BITMASK == b->getCollisionBitmask())
        || ( BIRD_COLLISION_BITMASK == b->getCollisionBitmask() && SCORE_POINT_COLLISION_BITMASK == a->getCollisionBitmask()))
    {
        score++;
        
        __String *temp_scoreString= __String::createWithFormat( "%u", score );
        scoreLabel->setString( temp_scoreString->getCString() );
    }
    
    return true;
}

bool GameScene::onTouchBegan( cocos2d::Touch *touch, cocos2d::Event *event )
{
    bird.Fly( );
    
    return true;
}