#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__

#include "cocos2d.h"
#include "Pipe.h"
#include "Bird.h"

class GameScene : public cocos2d::Layer
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();
    CREATE_FUNC(GameScene);

    void Fly( float dt ) { bird.Fly(); };
    
private:
    void SpawnPipe ( float dt ) { pipe.SpawnPipe( this ); };
    void SpawnBird ( cocos2d::Layer *layer) { bird.createBird( this ); };
    
    void SetPhysicsWorld( cocos2d::PhysicsWorld *world ){ sceneWorld = world; };
    
    bool onContactBegin( cocos2d::PhysicsContact &contact );
    bool onTouchBegan( cocos2d::Touch *touch, cocos2d::Event *event );
    
    cocos2d::PhysicsWorld *sceneWorld;
    cocos2d::Label *scoreLabel;
    
    Pipe pipe;
    Bird bird;
    
    unsigned int score;
};

#endif // __GAME_SCENE_H__
