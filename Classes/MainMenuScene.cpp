#include "MainMenuScene.h"
#include "SimpleAudioEngine.h"
#include "GameScene.h"
#include "Definitions.h"

USING_NS_CC;

Scene* MainMenuScene::createScene()
{
    auto scene = Scene::create();
    auto layer = MainMenuScene::create();
    scene->addChild(layer);

    return scene;
}

bool MainMenuScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto backgroundSprite = Sprite::create( "iphonehd/Menu_Background.png" );
    backgroundSprite->setPosition( Point( visibleSize.width/2 + origin.x, visibleSize.height/2 + origin.y ) );
    
    this->addChild( backgroundSprite );
    
/*UFO Sprite*/
    auto UFOSprite = Sprite::create("iphonehd/UFO.png");
    UFOSprite->setPosition( Vec2( visibleSize.width / 2 + origin.x, visibleSize.height * 0.8 + origin.y ) );
    this->addChild(UFOSprite);
    
/*GameNameLabel*/
    auto GameNameLabel = Label::createWithTTF( "space maneuver", "fonts/Roboto-Light.ttf",
                                              visibleSize.height * 0.04 );
    GameNameLabel->setPosition( Point( UFOSprite->getPositionX(),
                                      UFOSprite->getPositionY() - UFOSprite->getContentSize().height / 2 - GameNameLabel->getContentSize().height ));
    
    GameNameLabel->setCascadeOpacityEnabled( true );
    this->addChild(GameNameLabel);
    
    //    auto fadeOut = FadeOut::create(TRANSITION_TIME);
    //    GameNameLabel->runAction(fadeOut);
    
/*play in scene with manually created physics*/
    auto playItem = MenuItemImage::create( "iphonehd/Manual_Button.png", "iphonehd/Manual_Button_Clicked.png",
                                          CC_CALLBACK_1(MainMenuScene::GoToGameScene, this ) );
    playItem->setPosition( Point( visibleSize.width / 2 + origin.x, visibleSize.height * 0.4 + origin.y ) );
    
    auto playBox2DItem = MenuItemImage::create( "iphonehd/Box2D_Button.png", "iphonehd/Box2D_Button_Clicked.png",
                                               CC_CALLBACK_1(MainMenuScene::GoToGameScene, this ) );
    
    playBox2DItem->setPosition( Point( visibleSize.width / 2 + origin.x,
                                      playItem->getPositionY() - playBox2DItem->getContentSize().height * 2));

    auto menu = Menu::create( playItem, playBox2DItem, NULL );
    menu->setPosition( Point::ZERO );
    
    this->addChild( menu );
    
    return true;
}

void MainMenuScene::GoToGameScene( cocos2d::Ref *sender )
{
    auto scene = GameScene::createScene();
    Director::getInstance()->replaceScene( TransitionFade::create( TRANSITION_TIME, scene ) );
}