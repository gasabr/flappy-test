#include "Pipe.h"
#include "SimpleAudioEngine.h"
#include "Definitions.h"

USING_NS_CC;

Pipe::Pipe()
{
    visibleSize = Director::getInstance()->getVisibleSize();
    origin = Director::getInstance()->getVisibleOrigin();
}

void Pipe::SpawnPipe( cocos2d::Layer *layer )
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    
    auto topPipe = Sprite::create( "iphonehd/Meteorits.png" );
    auto bottomPipe = Sprite::create( "iphonehd/Meteorits.png" );
    
/*Randomize spawn position*/
    auto random = CCRANDOM_0_1();
    
/*If position is out of range - set min or max*/
    if ( random > UPPER_SCREEN_PIPE_THRESHOLD )
        random = UPPER_SCREEN_PIPE_THRESHOLD;
    else if ( random < BOTTOM_SCREEN_PIPE_THRESHOLD )
        random = BOTTOM_SCREEN_PIPE_THRESHOLD;
    
    auto topPipeBody = cocos2d::PhysicsBody::createBox( topPipe->getContentSize() );
    auto bottomPipeBody = cocos2d::PhysicsBody::createBox( bottomPipe->getContentSize() );
    
    topPipeBody->setDynamic( false );
    bottomPipeBody->setDynamic( false );
    
/*Set collision and contact bitmask*/
    topPipeBody->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    bottomPipeBody->setCollisionBitmask( OBSTACLE_COLLISION_BITMASK );
    topPipeBody->setContactTestBitmask( true );
    bottomPipeBody->setContactTestBitmask( true );
    
    topPipe->setPhysicsBody( topPipeBody );
    bottomPipe->setPhysicsBody( bottomPipeBody );
    
    auto topPipePosition = visibleSize.height * random + ( topPipe->getContentSize().height / 2 );
    
/*Create ball to calculate gap between pipes*/
    auto birdSprite = Sprite::create( "iphonehd/UFO.png" );
    auto birdHeight = birdSprite->getContentSize().height;
    auto gap = birdHeight * PIPE_GAP;
    
    topPipe->setPosition( Point( visibleSize.width + topPipe->getContentSize( ).width + origin.x, topPipePosition) );
    bottomPipe->setPosition( Point( topPipe->getPositionX() ,
                                   topPipePosition - gap - bottomPipe->getContentSize().height ) );
    
    layer->addChild( topPipe );
    layer->addChild( bottomPipe );
    
/*Actions*/
    auto topPipeAction = MoveBy::create( PIPE_MOVEMENT_SPEED * visibleSize.width, Point( -visibleSize.width * 1.5, 0 ) );
    auto bottomPipeAction = MoveBy::create( PIPE_MOVEMENT_SPEED * visibleSize.width, Point( -visibleSize.width * 1.5, 0 ) );
    
    topPipe->runAction( topPipeAction );
    bottomPipe->runAction( bottomPipeAction );

/*Score point*/
    auto scorePointNode = Node::create();
    auto scorePointBody = PhysicsBody::createBox( Size( 1, gap ) );
    
    scorePointBody->setDynamic( false );
    scorePointBody->setCollisionBitmask( SCORE_POINT_COLLISION_BITMASK );
    scorePointBody->setContactTestBitmask( SCORE_POINT_CONTACT_MASK );
    
    scorePointNode->setPhysicsBody( scorePointBody );
    scorePointNode->setPosition( topPipe->getPositionX(),
                                topPipe->getPositionY() - topPipe->getContentSize().height / 2 - gap / 2);
    
    layer->addChild( scorePointNode );
    
    auto scorePointAction = MoveBy::create( PIPE_MOVEMENT_SPEED * visibleSize.width, Point( -visibleSize.width * 1.5, 0 ) );
    
    scorePointNode->runAction( scorePointAction );
}