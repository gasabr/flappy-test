#include "SplashScene.h"
#include "SimpleAudioEngine.h"
#include "MainMenuScene.h"
#include "Definitions.h"

USING_NS_CC;

Scene* SplashScene::createScene()
{
    auto scene = Scene::create();
    auto layer = SplashScene::create();
    scene->addChild(layer);

    return scene;
}

bool SplashScene::init()
{
    if ( !Layer::init() )
    {
        return false;
    }
    
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

/*Background Image*/
    auto sprite = Sprite::create("iphonehd/Menu_Background.png");
    sprite->setPosition( Vec2( visibleSize.width / 2 + origin.x, visibleSize.height / 2 + origin.y ) );
    this->addChild(sprite);
    
/*UFO Sprite*/
    auto UFOSprite = Sprite::create("iphonehd/UFO.png");
    UFOSprite->setPosition( Vec2( visibleSize.width / 2 + origin.x, visibleSize.height * 0.8 + origin.y ) );
    this->addChild(UFOSprite);
    
/*GameNameLabel*/
    auto GameNameLabel = Label::createWithTTF( "space maneuver", "fonts/Roboto-Light.ttf",
                                              visibleSize.height * 0.04 );
    GameNameLabel->setPosition( Point( UFOSprite->getPositionX(),
                        UFOSprite->getPositionY() - UFOSprite->getContentSize().height / 2 - GameNameLabel->getContentSize().height));
    
    GameNameLabel->setCascadeOpacityEnabled( true );
    this->addChild(GameNameLabel);
    
    this->scheduleOnce( schedule_selector( SplashScene::GoToMainMenuScene ), DISPLAY_TIME_SPLASH_SCENE );
    
    return true;
}

void SplashScene::GoToMainMenuScene( float dt )
{
    auto scene = MainMenuScene::createScene();
    Director::getInstance()->replaceScene( scene );
}
